// // //boucle for

for (let i = 0; i <= 5; i++) {
    console.log(`la valeur est: ${i}`);
}

// //parcourire un tableau avec for

let array = ['mangues', 'pommes', 'oranges', 'fraises'];

console.log('---- boucle for ----')
for (let i = 0; i < array.length; i++) {
    console.log(array[i])
}

// //parcourire un tableau avec while

let j = 0;

console.log('---- boucle while ----')
while (j < array.length) {
    console.log(array[j]);
    j++;
}

// //parcourire un tableau avec Do while

let a = 0;

console.log('---- boucle do while ----')
do {
    console.log(array[a]);
    a++;
} while (a < array.length);


// //for in: itere sur les cles d'un odjet

console.log('---- boucle for in ----')

voiture = {
    marque: 'audi',
    couleur: 'noire',
    annee: 2022,
}

for (let k in voiture) {
    
    console.log(k + ": " + voiture[k]);

}

// //for of: itere sur les valeures d'un tableau

console.log('---- boucle for of ----');

let array1 = ['mangue', 'pomme', 'orange', 'fraise'];

for (let k of array1) {
    
    console.log(k);

}


// EXERCICE 1
//Écrire un programme qui permet à un utilisateur de voir quelques types de boucles, avec un champ où écrire un type de boucle et de voir la définition et l’utilisation de cette boucle.

// alert('Les types de boucles sont:' + 
// ' 1- for' +
// ' 2- while'+
// ' 3- do while'+
// ' 4- for in'+
// ' 5- for of');

// let boucle = prompt('Veuillez entrer le nom de votre boucle.').toLowerCase();

// def = {
//     for: 'La boucle for est utilisée pour exécuter un bloc de code spécifié un certain nombre de fois.',
//     while: 'La boucle while est utilisée pour exécuter un bloc de code tant qu\'une condition spécifiée est vraie.',
//     dowhile: 'La boucle do-while est similaire à la boucle while, mais elle exécute d\'abord un bloc de code une fois, puis vérifie la condition.'
// }

// uti = {
//     for: 'for (let i = 0; i < 10; i++) {'+
//     'console.log(i);'+
//     '}'+
//     'Cela imprimera les nombres de 0 à 9 sur la console.',
//     while: 'let i = 0;' + 
//     'while (i < 7) {' + 
//     '  console.log(i);'+
//     '  i++;'+ 
//     '}'+
//     'Cela imprimera les nombres de 0 à 6 sur la console.',
//     dowhile: 'let i = 0;'+
//     'do {'+
//     '  console.log(i);'+
//     '  i++;'+
//     '} while (i < 5);'+
//     'Cela imprimera les nombres de 0 à 4 sur la console.',
// }

// switch (boucle) {
//     case 'for':{
//         alert('Defintion de for : ' +
//         def.for +
//         '  Utilisation :: '+
//         uti.for);
//         break;
//     }
        
//     case 'while':{
//         alert('Defintion de while : ' +
//         def.while +
//         '  Utilisation :: '+
//         uti.while);
//         break;
//     }
        
//     case 'do while':{
//         alert('Defintion de do while : '  +
//          def.dowhile +
//          '  Utilisation :: ' +
//          uti.dowhile);
//         break;
//     }
        
//     case 'for in':{
//         alert('Defintion de for in : ');
//         break;
//     }
        
//     case 'for of':{
//         alert('Defintion de for of : ');
//         break;
//     }
            
//     default:{
//         alert('boucle non identifier')
//         break;
//     }
        
// }


//EXECICE 2 : table de multiplication

// alert('Entrer une valeur pour afficher sa table de multiplication');

// let n = prompt("Veuillez saisir votre valeur");

// if (Number.isInteger(parseInt(n)) && n > 0) {
//     for (let i = 0; i <= 10; i++) {
//         console.log(`${n} * ${i} = ${n * i}`)
        
//     }
// }else{
//     alert('Veuillez entrer une valeur entier positive!')
// }



//boucle while

// let v = parseInt(prompt('Veuillez saisir votre valeur'));



// if (isNaN(v) || v <= 0) {
//     alert('Erreur: Veuillez entrer un nombre entier positive!');
// } else {

//     let i = 0;
//     while (i <= 10) {
//         console.log(`${v} * ${i} = ${v * i}`);
//         i++;
//     }
// }

